/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author gpatel
 *
 */
public class Fahrenheit extends Temperature {
	public Fahrenheit(float t) {
		super(t);
	}
	public String toString(){
		return "" + value + " F";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub;
		value -= 32;
		value *= .55555555555555555555555555555;
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		this.toCelsius();
		value += 273;
		return this;
	}
}
