package edu.ucsd.cs110w.temperature;

public class Kelvin extends Temperature {
	public Kelvin(float t)
	{
		super(t);
	}

	public String toString() {
		// TODO: Complete this method
		return "" + value + " K";
	}

	@Override
	public Temperature toCelsius() {
		// TODO: Complete this method
		value -= 273;
		return this;
	}

	@Override
	public Temperature toFahrenheit() {
		// TODO: Complete this method
		this.toCelsius();
		value *= 1.8;
		value += 32;
		return this;
	}
	
	@Override
	public Temperature toKelvin() {
		// TODO: Complete this method
		return this;
	}
}
