/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author gpatel
 *
 */
public class Celsius extends Temperature {
	public Celsius(float t) {
		super(t);
	}
	public String toString() {
		return "" + value + " C";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		value *= 1.8;
		value += 32;
		return this;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		value += 273;
		return this;
	}
}
